
CREATION du fichier des G (JMAX=199)
Matrice d'orientation dans Td

* MAPLE sur les machines DEC du CRI
  voir les jobs et resultats dans ./MAPLE
  bsub -q s15j djmmp.com		pour lancer le job djmmp
  bjobs					pour voir son etat

  resultat de l'execution	dans djmmp.out
  resultat du calcul		dans testFile (60Mo)

* creation du fichier de G a partir de testFile
  ajuster la valeur de J dans calQG.f
  compiler les .f avec compile	(necessite un compilateur
				quadruple precision)
  calQG.x		<< ../MAPLE/testFile
			>> GJCMS.BD
			>> ligne
  triG.x		<< GJCMS.BD
			<< ligne	= nbgxxx
			>> TEST.BD	= gjcxxx.bd
  compG.x pour comparer 2 fichiers de G
			<< TEST.BD
			<< gjc96.bd
			<< nbg96
			>>compG.out
  icod.x pour valeurs maxi de MP et NMB
			<< GJCMS.BD
			>> icod.out

