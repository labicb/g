      PROGRAM CALCG
C
C  Michael Rey
C  Ch.Wenger mars 2001
C
      PARAMETER (JMAX=199,NBDCM=1353400)
CCC      PARAMETER (JMAX=10,NBDCM=1353400)
      IMPLICIT REAL*16 (A-H,O-Z)
C
      DIMENSION GM(3000,4),NA1EM(4),NA1M(4)
      COMMON /EMRSO3/ DC(NBDCM),NODJ(JMAX+1),NODM1(JMAX+1)
C
77    FORMAT(I10)       
1000  FORMAT(I5,X,I5,X,I5,2X,D25.12)
1001  FORMAT(I5,X,I5,X,I5,2X,ES25.12)
C
      IGREC=0
      JMIN=0
      ACC=1.Q-16
C
C traitement du fichier des D calcule par MAPLE (djmmp)
C
      OPEN(10,FILE='../MAPLE/testFile')
C
C position relative des D en fonction de M1
C
      NODM1C=0
      DO M1=0,JMAX
        NODM1(M1+1)=NODM1C
        DO M2=0,M1
          NODM1C=NODM1C+1
        ENDDO
      ENDDO
C
C position relative des D en fonction de J
C lecture des D
C
      NBDC=1
      DO 1 J=0,JMAX
        NODJ(J+1)=NBDC
        DO 2 M1=0,J
          DO 3 M2=0,M1
            IF(NBDC.GT.NBDCM) THEN
              WRITE(*,*) 'Depassement de NBDCM'
              GOTO 999
            ENDIF
            READ(10,1000,ERR=998) JC,M1C,M2C,DCC
            DC(NBDC)=DCC
            NBDC=NBDC+1
3         CONTINUE
2       CONTINUE
1     CONTINUE
      CLOSE(10)
C
      OPEN(11,FILE='GJCMS.BD')
      CLOSE(11,STATUS='DELETE')
      OPEN(11,FILE='GJCMS.BD',ACCESS='DIRECT',RECL=16)
      OPEN(13,FILE='ligne')
      IIL=1
      WRITE(13,77) IIL
      DO J=JMIN,JMAX
        CALL AE12(J,NA1EM,NA1M,GM,ACC)
C
        CALL GMATR(J,NA1EM,NA1M,GM,ACC,IGREC)
        CALL GFMAT(J,ACC,IGREC)
C
        WRITE(13,77) IGREC+1
C
      END DO
      CLOSE(13)
      CLOSE(11)
      GOTO 9000
998   WRITE(*,*) 'Fin prematuree de la lecture'
999   WRITE(*,*) 'ARRET SUR ERREUR dans le fichier des D'
9000  END
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      SUBROUTINE AE12(J,NA1EM,NA1M,GM,ACC)
      IMPLICIT REAL*16 (A-H,O-Z)
      DIMENSION A1E(3000),A2E(3000),B(3000),NA1EM(4),NA1M(4)
      DIMENSION GM(3000,4)
      COMMON /PGM/ B,A1E
C
      CALL AEM(J,NA1E,NA2E,A1E,A2E)
C       
C ----> NA1E : nb de composantes a1 et e1 dans D(j) 
C ----> NA2E : nb de composantes a2 et e2 dans D(j)
C
      IQ=MOD(J,12)
      IR=0
      IF(MOD(IQ,2).EQ.0) IR=1
      IF(IQ.EQ.9) IR=1
      IF(IQ.EQ.2) IR=0
C       
C ----> multiplicites de a1,a2 et e dans D(j) 
C
      NA1=(J/12)+IR
      NE=NA1E-NA1
      NA2=NA2E-NE
C--------------------------------------------------------       
C
      NA1EM(1)=NA1E
      NA1EM(2)=NA1E
      NA1EM(3)=NA2E
      NA1EM(4)=NA2E
C
      NA1M(1)=NA1
      NA1M(2)=NE
      NA1M(3)=NE
      NA1M(4)=NA2
C
      NSA1=0
      NSA2=0
      NSE1=0
      NSE2=0
C
C-----------------------------------------------       
C            cas d'une composante 1
C-----------------------------------------------
      IF(NA1E.EQ.0) GO TO 1
C
      CALL DMSPR(A1E,B,NA1E,0,ACC)
C       
C-------> "B" : vecteurs propres
C-------> A1E=1. pour A1
C       
      DO 5 K=1,NA1E
        V2=A1E((K*K+K)/2)
C
        IF(ABS(V2-1.Q0).LT.1.Q-3) GO TO 6
C
        DO I=1,NA1E
          GM(NSE1+I,2)=B(I+NA1E*(K-1))
        END DO
        NSE1=NSE1+NA1E
        GO TO 5
C
6       DO I=1,NA1E
          GM(NSA1+I,1)=B(I+NA1E*(K-1))
        END DO
        NSA1=NSA1+NA1E
C
5     CONTINUE
1     IF(NA2E.EQ.0) GO TO 2
C------------------------------------------------       
C            cas d'une composante 2
C------------------------------------------------
      CALL DMSPR(A2E,B,NA2E,0,ACC)
C       
C-------> "B" : vecteurs propres
C-------> A2E=-1. pour A2
C       
      DO 8 K=1,NA2E
        V2=A2E((K*K+K)/2)
C
        IF(ABS(V2+1.Q0).LT.1.Q-3) GO TO 7
C
        DO I=1,NA2E
          GM(NSE2+I,3)=B(I+NA2E*(K-1))
        END DO
        NSE2=NSE2+NA2E
        GO TO 8
C
7       DO I=1,NA2E
          GM(NSA2+I,4)=B(I+NA2E*(K-1))
        END DO
        NSA2=NSA2+NA2E
C
8     CONTINUE
2     RETURN
      END
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      SUBROUTINE AEM(J,N1,N2,A1E,A2E)
      IMPLICIT REAL*16 (A-H,O-Z)
      DIMENSION A1E(3000),A2E(3000)
      VV=2.Q0*(.5Q0)**J
      M=J
      N1=0
      N2=0
      IF(J.EQ.1) GO TO 7
      IC1=2*MOD(J,2)+1
      C1=SQRT(2.Q0)
      GO TO 2
1     VV=-VV*SQRT(REAL(J+M,16)/REAL(J-M+1,16))
      M=M-1
2     IF(MOD(M,2).EQ.1) GO TO 1
      V1=VV
      IF(M.EQ.0) V1=VV/C1
      V2=0.Q0
      M1=J
      GO TO 4
3     VX=2.Q0*M*V1/SQRT(REAL(J+M1,16)*REAL(J-M1+1,16))-
     * V2*SQRT(REAL(J-M1,16)
     * *REAL(J+M1+1,16)/REAL(J+M1,16)/REAL(J-M1+1,16))
      V2=V1
      V1=VX
      M1=M1-1
4     IF(MOD((M1-M),4).GT.0) GO TO 3
      IF(MOD(M,4).EQ.0) GO TO 5
      N2=N2+1
      A2E(N2)=V1
      GO TO 6
5     N1=N1+1
      IF(M1.EQ.0) V1=V1/C1
      A1E(N1)=V1
6     IF(M1.NE.M) GO TO 3
      IF((M-IC1).NE.-1) GO TO 1
      N1=(J/4)+MOD((J+1),2)
      N2=((J+2)/4)
7     RETURN
      END
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C   **** CALCUL DE LA MATRICE POUR LES A1, A2, ET E ****
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      SUBROUTINE GMATR(J,NA1EM,NA1M,GM,ACC,IGREC)
      IMPLICIT REAL*16 (A-H,O-Z)
      DIMENSION B(3000),C(3000),G(3000),F(3000),NA1EM(4),NA1M(4)
      DIMENSION GM(3000,4)
      COMMON /PGM/ B,F
C
      C1=SQRT(7.Q0/12.Q0)
      C2=SQRT(5.Q0/24.Q0)
      C4=SQRT(350.Q0/24.Q0)
C
      DO L=1,4
        IF(L.LE.2) THEN
          KMAX1=J-MOD(J,4)
        ELSE
          KMAX1=J-MOD(J+2,4)
        END IF
C
        NA1E=NA1EM(L)
        NA1=NA1M(L)
        L1=L
        IF(L.GE.2) L1=L+1
        IF(L.EQ.4) L1=2
        IF(NA1.EQ.0) GO TO 14
        KD=4*MOD(J,2)
        IF(L.GT.2) KD=2
        C3=1.Q0
        DO K=1,4
          C3=C3*REAL(2*J+1-K,16)*REAL(2*J+K,16)
        END DO
        C3=C3*(2*J+5)
        C3=SQRT(C3)
        IF(J.LT.2) C3=1.Q0
        MAX=NA1E*NA1
        DO N=1,MAX
          B(N)=GM(N,L)
        END DO
        N=0
        DO 11 N1=1,NA1
          DO 11 N2=1,N1
            N=N+1
            F1=0.Q0
            DO 10 KV=1,NA1E
              K=4*(NA1E-KV)+KD
              FI=C1*2.Q0*(3.Q0*(J*(J+2)-5.Q0*K*K)*(J*J-5.Q0*K*K-1.Q0)
     *           -10.Q0*K*K*(4.Q0*K*K-1.Q0))/C3
C
              IF(K.NE.2) GO TO 8
              FIX=C4*(J-1)*J*(J+1)*(J+2)/C3
              FI=FI+(1.Q0-2.Q0*MOD(J,2))*FIX
8             F1=F1+B(KV+NA1E*(N1-1))*B(KV+NA1E*(N2-1))*FI
C
              IF(KV.EQ.1) GO TO 10
              FI=70.Q0
              DO 9 M=1,4
9               FI=FI*REAL(J+K+M,16)*REAL(J-K+1-M,16)
              FI=SQRT(FI)/C3
C
              IF(K.EQ.0) FI=FI*SQRT(2.Q0)
C
              F1=F1+(B(KV-1+NA1E*(N2-1))*B(KV+NA1E*(N1-1))
     *           +B(KV+NA1E*(N2-1))*B(KV-1+NA1E*(N1-1)))*FI*C2
C
10          CONTINUE
11      F(N)=F1
C
C  ----------> diagonalisation
C
        CALL DMSPR(F,C,NA1,0,ACC)
        CALL EXCH(NA1,F,C)
C
        DO 13 N=1,NA1
          DO 13 K=1,NA1E
            G1=0.Q0
            DO 12 N1=1,NA1
12            G1=G1+B(K+NA1E*(N1-1))*C(N1+NA1*(N-1))
            KV=4*(NA1E-K)+KD
            IF(KV.NE.0) G1=G1/SQRT(2.Q0)
13          G(K+NA1E*(N-1))=G1
        DO III=1,NA1
          KMAX=KMAX1
          CALL OUTPUT(J,III,G(NA1E*(III-1)+1),NA1E,KMAX,L1,IGREC)
        END DO
14      CONTINUE
      END DO
      RETURN
      END
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C             **** CALCUL DES G POUR F1Z ET F2Z ****
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      SUBROUTINE GFMAT(J,ACC,IGREC)
      IMPLICIT REAL*16 (A-H,O-Z)
C      JMAX=200
C     DIMENSION C(3000),F(1275)
      DIMENSION C(3000),F(3000)
C
      C1=SQRT(7.Q0/12.Q0)
      C2=SQRT(5.Q0/24.Q0)
      C4=SQRT(350.Q0/24.Q0)
      DO 15 L=1,2
        IF(L.EQ.1) THEN
          KMAX1=J-MOD(J,4)
        ELSE
          KMAX1=J-MOD(J+2,4)
        END IF
        GO TO (1,2),L
1       NA1=(J/4)+MOD(J,2)
        GO TO 5
2       NA1=(J+2)/4
5       NA1E=NA1
        L1=L+4
        IF(NA1.EQ.0) GO TO 14
        KD=4*MOD((J+1),2)
        IF(L.EQ.2) KD=2
        C3=1.Q0
        DO 6 K=1,4
6         C3=C3*REAL(2*J+1-K,16)*REAL(2*J+K,16)
        C3=C3*(2*J+5)
        C3=SQRT(C3)
        IF(J.EQ.1) C3=1.Q0
        N=0
        DO 11 N1=1,NA1
          DO 11 N2=1,N1
            N=N+1
            IF(N1-N2-1) 7,3,10
7           K=4*(NA1E-N2)+KD
            IF(J.LT.2) GO TO 10
            FI=C1*2.Q0*(3.Q0*(J*(J+2)-5.Q0*K*K)*(J*J-5.Q0*K*K-1.Q0)
     *        -10.Q0*K*K*(4.Q0*K*K-1.Q0))/C3
C
            IF(K.NE.2) GO TO 8
            FIX=C4*(J-1)*J*(J+1)*(J+2)/C3
            FI=FI-(1.Q0-2.Q0*MOD(J,2))*FIX
C
8           GO TO 11
3           K=4*(NA1E-N2-1)+KD
            FI=70.Q0
            DO 9 M=1,4
9             FI=FI*REAL(J+K+M,16)*REAL(J-K+1-M,16)
            FI=C2*SQRT(FI)/C3
C
            IF(K.EQ.0) FI=FI*SQRT(2.Q0)
C
            GO TO 11
10          FI=0.Q0
11          F(N)=FI
C
C ----------> diagonalisation
C
        CALL DMSPR(F,C,NA1,0,ACC)
        CALL EXCH(NA1,F,C)
        DO 13 N=1,NA1
          DO 13 K=1,NA1E
            KV=4*(NA1E-K)+KD
            IF(KV.NE.0) C(K+NA1E*(N-1))=C(K+NA1E*(N-1))/SQRT(2.Q0)
13        CONTINUE
        DO III=1,NA1
          KMAX=KMAX1
C
          CALL OUTPUT(J,III,C(NA1E*(III-1)+1),NA1E,KMAX,L1,IGREC)
        END DO
14    CONTINUE
15    CONTINUE
      RETURN
      END
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      SUBROUTINE OUTPUT(J,III,G,MAX,KMAX,L1,IGREC)
      IMPLICIT REAL*16 (A-H,O-Z)
C
      CHARACTER*2 IC
      DIMENSION G(3000),IC(6),ICOD(3000)
      DATA IC/'A1','A2','E1','E2','F1','F2'/
C
2     FORMAT(I6,F15.13,2I8)
C
      PHI=G(1)/ABS(G(1))
C
      COEF=2.Q0/SQRT(3.Q0)
      PHIB=1.Q0
      IF(2*(J/2)-J.EQ.0) THEN
        IF(L1.GT.3) THEN
          PHIB =-1.Q0
        ENDIF
      ELSE
        IF(L1.LE.3)THEN
          PHIB=-1.Q0
        ENDIF
      ENDIF
C
      KKMAX=KMAX
      DO I=1,MAX
        IF(L1-4)10,11,11
10      LL1=L1
        GO TO 12
11      LL1=L1-1
12      NMAX=NSYM1(J,1,LL1)
        IF(2*(J/2)-J.EQ.0) THEN
          NMB=III-1
        ELSE
          NMB=NMAX-III
        ENDIF
        ICOD(I)=LL1+10*NMB+1000*KMAX
        G(I)=G(I)/PHI*PHIB
C
        IF(L1.NE.4) IGREC=IGREC+1
        IF(L1.NE.4) WRITE(11,REC=IGREC) ICOD(I),DBLE(G(I)),J
C
        KMAX=KMAX-4
      ENDDO
C
Ccccccccccccccccccccccccccc   ****** E2 ******       
      IF(L1.EQ.3) THEN
        MPMAX=MMAXS(J,3,2)
        DO 17 MP=2,MPMAX,4
          G1=0.Q0
          DO 16 IM=1,MAX
            MM=ICOD(IM)/1000
            CALL EMC3(J,MP,MM,D)
            G01=G(IM)
            IF(MM.EQ.0) THEN
              DG1=G01*D
              G1=G1+DG1
            ELSE
              DG1=G01*D
              G1=G1+DG1+DG1
            ENDIF
16        CONTINUE
          G1=COEF*G1
          IF((2*(J/2)-J.EQ.0).AND.(4*((MP+2)/4).EQ.0)) THEN G1=-G1
          ICOD1=1000*MP+10*NMB+3
          IGREC=IGREC+1
          WRITE(11,REC=IGREC)ICOD1,DBLE(G1),J
17      CONTINUE
      ENDIF
Ccccccccccccccccccccc ****** F1x *********       
      IF(L1.GT.4)GO TO 20
      RETURN
20    IF(L1.EQ.5) THEN
        MMAX=MMAXS(J,4,1)
        DO 27 MM=MMAX,0,-2
          G1=0.Q0
          DO 26 IM=1,MAX
            MP=ICOD(IM)/1000
            CALL EMC3(J,MM,MP,D)
            G01=G(IM)
            IF(MP.EQ.0) THEN
              DG1=G01*D
              G1=G1+DG1
            ELSE
              DG1=G01*D
              G1=G1+DG1+DG1
            ENDIF
26        CONTINUE
          G1=-G1
          ICOD1=1000*MM+10*NMB+4
          IGREC=IGREC+1
          WRITE(11,REC=IGREC) ICOD1,DBLE(G1),J
27      CONTINUE
      ENDIF
Cccccccccccccccccc ****** F2x *********         
      IF(L1.EQ.6) THEN
        MMAX=MMAXS(J,5,1)
        DO 37 MM=MMAX,0,-2
          G1=0.Q0
          DO 36 IM=1,MAX
            MP=ICOD(IM)/1000
            CALL EMC3(J,MM,MP,D)
            G01=G(IM)
            IF(MP.EQ.0) THEN
              DG1=G01*D
              G1=G1+DG1
            ELSE
              DG1=G01*D
              G1=G1+DG1+DG1
            ENDIF
36        CONTINUE
          G1=-G1
          ICOD1=1000*MM+10*NMB+5
          IGREC=IGREC+1
          WRITE(11,REC=IGREC)ICOD1,DBLE(G1),J
37      CONTINUE
      ENDIF
      RETURN
      END
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      SUBROUTINE EXCH(N,F,C)
      IMPLICIT REAL*16 (A-H,O-Z)
      DIMENSION F(3000),C(3000)
      IF(N.EQ.1) GO TO 4
      DO 3 L=1,(N-1)
        L1=(L*L+L)/2
        AM=F(L1)
        DO 2 M=(L+1),N
          M1=(M*M+M)/2
          X=F(M1)
          IF(X.GE.AM) GO TO 2
          AM=X
          F(M1)=F(L1)
          F(L1)=X
          DO 1 J=1,N
            VX=C(J+N*(M-1))
            C(J+N*(M-1))=C(J+N*(L-1))
            C(J+N*(L-1))=VX
1         CONTINUE
2       CONTINUE
3     CONTINUE
4     RETURN
      END
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C      SUBROUTINE DMSPRO
C
C     THIS SUBROUTINE IMPLEMENTS THE METHOD OF JACOBI FOR
C     FINDING THE EIGENVALUES AND EIGENVECTORS OF AN (N*N)
C     REAL SIMMETRYC MATRIX A.
C
C     THE COMPUTATIONS ARE PERFORMED USING DOUBLE PRECISION
C     FLOATING POINT ARITHMETIC.
C
C     SUBROUTINE USAGE.
C
C     INPUT PARAMETERS:
C
C     A    :UPPER TRIANGULAR PART OF THE REAL SYMMETRIC
C           MATRIX A.DIMENSION OF THE ARRAY A IS N*(N+1)/2.
C
C     N    :THE ORDER OF THE MATRIX.
C
C     KOD  :KOD=0 ; EIGENVALUES AND EIGENVECTORS WILL BE
C           KOD=1 ; ONLY EIGENVALUES WILL BE COMPUTED.
C
C   EPS  :TOLERANCE ,USED IN TERMINATION CRITERION.
C
C     RESULTS:
C
C     A    :WHEN THE MATRIX A HAS BEEN REDUCED TO DIAGONAL
C           FORM,THE EIGENVALUES WILL BE FOUND IN THE DIA-
C           GONAL POSITIONS.
C
C     B    :ARRAY CONTAINING THE EIGENVECTORS OF THE MATRIX A.
C           DIMENSION OF THE ARRAY B IS N*N.
C
      SUBROUTINE DMSPR(A,B,N,KOD,EPS)
      IMPLICIT REAL*16 (A-H,O-Z)
      DIMENSION A(3000),B(3000)
      IF(KOD.EQ.1) GOTO 2
      I1=-N
      DO 1 J=1,N
        I1=I1+N
        DO 1 I=1,N
          IJ=I1+I
          B(IJ)=0.Q0
          IF(I.NE.J) GOTO 1
          B(IJ)=1.Q0
1     CONTINUE
C
2     RN=0.Q0
      DO 3 I=1,N
        DO 3 J=1,N
          IF(I.EQ.J) GOTO 3
          IJ=I+(J*J-J)/2
          RN=RN+A(IJ)**2
3     CONTINUE
      IF(RN.LE.EPS) GOTO 19
      RN=SQRT(2.Q0*RN)
      RX=RN*EPS/REAL(N,16)
C
      INT=0
      SEUIL=RN
4     SEUIL=SEUIL/REAL(N,16)
5     L=1
6     M=L+1
C
7     M1=(M*M-M)/2
      L1=(L*L-L)/2
      LM=L+M1
      IF(ABS(A(LM)).LT.SEUIL) GOTO 15
      INT=1
      LL=L1+L
      MM=M1+M
      P=.5Q0*(A(LL)-A(MM))
      Q=-A(LM)/SQRT(A(LM)**2+P**2)
      IF(P.GE.EPS) GOTO 8
      Q=-Q
8     S=Q/SQRT(2.Q0*(1.Q0+SQRT(ABS(1.Q0-Q**2))))
      SS=S*S
      C=SQRT(1.Q0-SS)
      CC=C*C
      SC=S*C
C
      IL1=N*L-N
      IM1=N*M-N
      DO 14 I=1,N
        I1=(I*I-I)/2
        IF(I.EQ.L) GOTO 13
        IF(I-M) 9,13,10
9       IM=M1+I
        GOTO 11
10      IM=I1+M
11      IF(I.GE.L) GOTO 111
        IL=L1+I
        GO TO 12
111     IL=I1+L
12      P=A(IL)*C-A(IM)*S
C
        A(IM)=A(IL)*S+A(IM)*C
        A(IL)=P
C
13      IF(KOD.EQ.1) GOTO 14
        ILB=IL1+I
        IMB=IM1+I
        P=B(ILB)*C-B(IMB)*S
        B(IMB)=B(ILB)*S+B(IMB)*C
        B(ILB)=P
14    CONTINUE
      P=2.Q0*A(LM)*SC
      Q=A(LL)*CC+A(MM)*SS-P
      P=A(LL)*SS+A(MM)*CC+P
      A(LM)=(A(LL)-A(MM))*SC+A(LM)*(CC-SS)
      A(LL)=Q
      A(MM)=P
C
15    IF(M.EQ.N) GOTO 16
      M=M+1
      GO TO 7
16    IF(L.EQ.(N-1)) GOTO 17
      L=L+1
      GOTO 6
17    IF(INT.NE.1) GOTO 18
      INT=0
      GOTO 5
18    IF(SEUIL.GT.RX) GOTO 4
C
19    CONTINUE
      RETURN
      END
C
C     multiplicites de a1,a2,e,f1 et f2 dans d**j(u ou g)
C
      SUBROUTINE NBJC(IR,IUG,NA1,NA2,NE,NF1,NF2) 
Csmil champion dec 78
      DIMENSION N(12)
      DATA N/10000,10,101,1011,10111,121,11112,1122,10222,11132,11223,
     &1233/
      IP=IR/12
      IR1=IR-12*(IR/12)+1
      NB=N(IR1)
      NA1=NB/10000
      NA2=(NB-NA1*10000)/1000
      NE=(NB-NA1*10000-NA2*1000)/100
      NF1=(NB-NA1*10000-NA2*1000-NE*100)/10
      NF2=NB-NA1*10000-NA2*1000-NE*100-NF1*10
      NA1=NA1+IP
      NA2=NA2+IP
      NE=NE+2*IP
      NF1=NF1+3*IP
      NF2=NF2+3*IP
      IF(IUG-1)5,5,6
6     NM=NA2
      NA2=NA1
      NA1=NM
      NM=NF2
      NF2=NF1
      NF1=NM
5     RETURN
      END
C
C     multiplicite de ic dans d**j(u ou g)
C
      FUNCTION NSYM1(J,IUG,IC) 
Csmil h.berger dec 78
      DIMENSION N(5)
      CALL NBJC(J,IUG,N1,N2,N3,N4,N5)
      N(1)=N1
      N(2)=N2
      N(3)=N3
      N(4)=N4
      N(5)=N5
      NSYM1=N(IC)
      RETURN
      END
C
C **********************************************************************
C
      SUBROUTINE EMC3(J,M1,M2,D)
C
C     elements matriciels des representations de SO(3) (operation C3)
C
      IMPLICIT REAL*16 (A-H,O-Z)
C
      IF(M2.GT.M1) THEN
        D=((-1)**(M1-M2))*EMC3A(J,M2,M1)
      ELSE
        D=EMC3A(J,M1,M2)
      ENDIF
      IFI=M2
      IFI=IFI-4*(IFI/4)
      IF(IFI)10,20,20
10    IFI=IFI+40
20    IF(IFI/2)40,40,30
30    D=-D
40    CONTINUE
      RETURN
      END
C
      FUNCTION EMC3A(J,M1,M2)
C
      PARAMETER (JMAX=199,NBDCM=1353400)
CCC   PARAMETER (JMAX=10,NBDCM=1353400)
      IMPLICIT REAL*16 (A-H,O-Z)
      COMMON /EMRSO3/ DC(NBDCM),NODJ(JMAX+1),NODM1(JMAX+1)
C
      EMC3A=DC(NODJ(J+1)+NODM1(M1+1)+M2)
      RETURN
      END
C
C **********************************************************************
C
      FUNCTION MMAXS(J,IC,IS)
C
C     valeur maximale de m du symbole G**J.
C
      IF(IC-3)1,2,4
1     IC1=IC
      GO TO 5
2     IF(IS-2)1,3,3
3     IC1=4
      GO TO 5
4     IF(IS-2)8,8,1
5     I=2*(IC1/2)-IC1
      IF(I)6,7,6
6     MMAXS=4*(J/4)
      RETURN
7     MMAXS=4*((J-2)/4)+2
      RETURN
8     MMAXS=2*((J-1)/2)+1
      RETURN
      END
