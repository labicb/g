      PROGRAM ICOD
C
C  valeurs maxi de MP et NMB
C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
C
      OPEN(12,FILE='GJCMS.BD',ACCESS='DIRECT',RECL=16)
      OPEN(14,FILE='icod.out')
C
C
      MPMAXD=-1
      NMBMAXD=-1
      MPMAXF=-1
      NMBMAXF=-1
      I=0
110   I=I+1
      READ(12,REC=I,ERR=111)ICODE,GMB,J
      MPC=ICODE/1000
      NMBC=(ICODE-MPC*1000)/10
      IF(MPC.GT.MPMAXD) THEN
        MPMAXD=MPC
        JMPD=J
      ENDIF
      IF(NMBC.GT.NMBMAXD) THEN
        NMBMAXD=NMBC
        JNMBD=J
      ENDIF
      IF(MPC.GE.MPMAXF) THEN
        MPMAXF=MPC
        JMPF=J
      ENDIF
      IF(NMBC.GE.NMBMAXF) THEN
        NMBMAXF=NMBC
        JNMBF=J
      ENDIF
      GOTO 110
111   CONTINUE
      WRITE(14,*) 'MPMAX  ',MPMAXD ,' pour 1er J ',JMPD ,
     &                              ', dernier J ',JMPF
      WRITE(14,*) 'NMBMAX ',NMBMAXD,' pour 1er J ',JNMBD,
     &                              ', dernier J ',JNMBF
      CLOSE(14)
      END
