      PROGRAM TRIG
C
C  trier les G issus de calcG.f
C
C Michael Rey
C Ch.Wenger 20.03.2001
C
C pour calcG avec 0,jmax => NBLM=jmax+2
C
      PARAMETER (NBGM=1333500 , NBLM=201)
      IMPLICIT DOUBLEPRECISION (A-H,O-Z)   
      DIMENSION G(NBGM),ICOD(NBGM),J(NBGM)
      DIMENSION GBIS(NBGM),ICODBIS(NBGM),JBIS(NBGM)
      DIMENSION K(NBGM)
      DIMENSION NUM(NBLM)
C
2     FORMAT(I10)
12    FORMAT(I6,1X,F15.13,I8)                       
C
      OPEN(11,FILE='GJCMS.BD',STATUS='OLD',ACCESS='DIRECT',RECL=16)
      OPEN(20,FILE='ligne',STATUS='OLD')
C
      IERREUR=0
C
C  **** lecture de la ligne ***
C       
      KK=0
50    KK=KK+1 
      READ(20,2,END=51) NUMC
      IF (KK.GT.NBLM) THEN
        IERREUR=1
      ELSE
        NUM(KK)=NUMC
      ENDIF
      GOTO 50
51    IK=KK-1
      IF (IERREUR.EQ.1) THEN
        WRITE(*,*) 'ERREUR : augmenter NBLM -> ', IK
        GOTO 9000
      ENDIF
C
C  **** lecture des G non ordonnes ***
C       
      I=0
100   I=I+1
      READ(11,REC=I,ERR=101) ICODC,GC,JC 
      IF (I.GT.NBGM) THEN
        IERREUR=1
      ELSE
        ICOD(I)=ICODC
        G(I)=GC
        J(I)=JC
      ENDIF
      GOTO 100           
101   II=I-1
      IF (IERREUR.EQ.1) THEN
        WRITE(*,*) 'ERREUR : augmenter NBGM -> ', II
        GOTO 9000
      ENDIF
C
      OPEN(40,FILE='TEST.BD')
      CLOSE(40,STATUS='DELETE')
      OPEN(40,FILE='TEST.BD',ACCESS='DIRECT',RECL=12)
CCC      OPEN(50,FILE='TEST.DAT')
      IGG=0
C  *** boucles sur les J ****   
      DO IZ=1,IK      
        IM=0
        DO JJ=1,II
          IF((JJ.GE.NUM(IZ)).AND.(JJ.LT.NUM(IZ+1))) THEN       
            IM=IM+1
            ICODBIS(IM)=ICOD(JJ)
            GBIS(IM)=G(JJ)
            JBIS(IM)=J(JJ)
          ENDIF
        ENDDO
        CALL ORDER(IM,ICODBIS,K)
        DO IU=1,IM
          IGG=IGG+1
CCC          WRITE(50,12) ICODBIS(K(IU)),GBIS(K(IU)),JBIS(K(IU))
          WRITE(40,REC=IGG) ICODBIS(K(IU)),GBIS(K(IU))
        ENDDO
      ENDDO 
C
      CLOSE(40) 
CCC      CLOSE(50) 
9000  END      
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C 
C *** CREATION D'UN TABLEAU D'ORDONNANCEMENT. 
C     LE TABLEAU ARGUMENT EST ENTIER
C 
C LABO SPECTRO C. MILAN MAI 75 
C
      SUBROUTINE ORDER(M,T1,IND)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z) 
      INTEGER T1,AK
      DIMENSION T1(*),IND(*) 
C
      N=1
      IND(1)=1 
      IF(M-1)16,16,1 
1     IT1=N+1
      AK=T1(IT1) 
      J=IND(1) 
      IF(AK-T1(J))2,2,11 
11    I1=1 
      I2=IT1 
3     I=(I1+I2)/2
      J=IND(I) 
      IF(AK-T1(J))12,4,4 
12    I2=I 
      GO TO 5
4     I1=I 
5     IF(I2-I1-1)3,6,3 
2     I2=1 
6     IF(I2-IT1)13,7,13
13    IR=N 
8     IT2=IR+1 
      IND(IT2)=IND(IR) 
      IF(I2-IR)14,7,14 
14    IR=IR-1
      GO TO 8
7     IND(I2)=IT1
      IF(M-IT1)15,16,15
15    N=N+1
      GO TO 1
16    RETURN 
      END
